def inputvalidation(a, b, p):
	validity = True
	power = -1
	for x in p:
		if x.isdigit() == True:
			power += 1
	print "Power of P:", power
	lista = a.split(" ")
	listb = b.split(" ")
	listp = p.split(" ")

	# No Negative Coefficients
	if "-" in lista == True or "-" in listb == True or "-" in listp == True:
		print "Negative Coefficient"
		validity = False

	# No Non-digit Input
	for x in lista:
		if x.isdigit() == False and len(x)>0:
			print "Non-digit Coefficient for A"
			validity = False
	for x in listb:
		if x.isdigit() == False and len(x)>0:
			print "Non-digit Coefficient for B"
			validity = False
	for x in listp:
		if x.isdigit() == False and len(x)>0:
			print "Non-digit Coefficient for P"
			validity = False

	# Coefficients should be less than the degree of P
	for x in lista:
		if (x.isdigit() == True and int(x) >= 2**power):
			print "Coefficient more than 2^Power(P)"
			validity = False
	for x in listb:
		if (x.isdigit() == True and int(x) >= 2**power):
			print "Coefficient more than 2^Power(P)"
			validity = False

	#Coefficients of Irreducible Polynomial should be binary
	for x in listp:
		if (x.isdigit() == True):
			if (int(x) != 1 and int(x) != 0):
				print "Non-binary Coefficient for P"
				validity = False

	return validity

def printpoly(x):
	power = len(x) - 1
	exponent = power
	ctr = -1
	poly = ""
	while exponent >= 0:
		ctr += 1
		if (exponent > 1 and x[ctr] != 0):
			poly += str(x[ctr])
			poly += "x^"
			poly += str(exponent)
			poly += " + "
		elif (exponent == 1 and x[ctr] != 0):
			poly += str(x[ctr])
			poly += "x"
			if(x[ctr+1] != 0 and ctr != len(x)):
				poly += " + "
		elif (exponent == 0):
			poly += str(x[ctr])
		exponent -= 1
	return poly

def stuff(a, b):
	maxlen = max(len(a), len(b))
	if len(a) != maxlen:
		stufflen = maxlen - len(a)
		for x in range (0, stufflen):
			a.insert(0, 0)
	else:
		stufflen = maxlen - len(b)
		for x in range (0, stufflen):
			b.insert(0, 0)
	return a, b

def add(a, b):
	print "Addition is performing bitwise XOR on every digit between A and B. P has nothing to do with addition."
	c = [0 for x in range (0, max(len(a), len(b)))]
	print " ",
	for x in a:
		print x, " ",
	print "\n+",
	for x in b:
		print x, " ",
	print "\n",
	hline = "---"
	for x in a:
		hline += "---"

	for x in range(0, max(len(a),len(b))):
		if x > len(a)-1:
			c[x] = 0 ^ b[x]
		elif x > len(b)-1:
			c[x] = a[x] ^ 0
		else:
			c[x] = a[x] ^ b[x]

	print hline
	print " ",
	for x in c:
		print x, " ",
	print "\n\nA(x)+B(x) = ", printpoly(c)

def subtract(a, b):
	print "Subtraction is performing bitwise XOR on every digit between A and B. P has nothing to do with addition."
	c = [0 for x in range (0, max(len(a), len(b)))]
	print " ",
	for x in a:
		print x, " ",
	print "\n-",
	for x in b:
		print x, " ",
	print "\n",
	hline = "---"
	for x in a:
		hline += "---"

	for x in range(0, max(len(a),len(b))):
		if x > len(a)-1:
			c[x] = 0 ^ b[x]
		elif x > len(b)-1:
			c[x] = a[x] ^ 0
		else:
			c[x] = a[x] ^ b[x]

	print hline
	print " ",
	for x in c:
		print x, " ",
	print "\n\nA(x)-B(x) = ", printpoly(c)

def digitmult(x, y, pint, pstr):
	print "Multiplying", x, y
	xstr = bin(x)[2:]
	ystr = bin(y)[2:]
	xint = int(xstr)
	yint = int(xstr)
	prod = int(bin(x)[2:])*int(bin(y)[2:])
	prodstr = str(prod)
	result = ""
	modprod = ""
	for i in prodstr:
		if int(i) > 1:
			modprod += str(int(i)%2)
		else:
			modprod += i
	prod = int(modprod)
	prodstr = modprod
	if (len(prodstr) >= len(pstr)):
		plis = [0 for i in range(0,len(prodstr))]
		prodlis = [0 for i in range(0,len(prodstr))]
		if prodstr[0] == '0':
			prodstr = prodstr[1:]
			for j in range(0,len(prodstr)):
				#print "XOR: ", int(prodstr[j]), int(pstr[j])
				result += str(int(prodstr[j]) ^ int(pstr[j]))
		else:
			pstr = pstr +"0"
			for j in range(0,len(prodstr)):
				#print "XOR: ", int(prodstr[j]), int(pstr[j])
				result += str(int(prodstr[j]) ^ int(pstr[j]))
	else:
		result = prodstr
	print "Result: ", int(result, 2)
	return int(result, 2)


def multiply(a, b, p):
	addproducts = [0 for i in range(0,len(a)+len(b)-1)]
	pstr = ""
	for x in p:
		pstr += str(x)
	print "P(x) in String:", pstr
	pint = int(pstr, 2)
	print "P(x) Binary in Decimal:", pint
	ctr = -1
	for x in b:
		ctr += 1
		ctr2 = ctr -1
		for y in a:
			ctr2 += 1
			addproducts[ctr2] = addproducts[ctr2] ^ digitmult(x,y,pint, pstr)
	print "Result: ", addproducts
	print "\n\nA(x)*B(x) mod P(x) = ", printpoly(addproducts)

decision = 1
while(decision != 5):
	decision = input("\n\n[1] Add [2] Subtract [3] Multiply [4] Divide [5] Exit\nInput: ")
	if(decision == 5):
		exit()
	araw = raw_input("Enter A's Coefficients: ")
	braw = raw_input("Enter B's Coefficients: ")
	praw = raw_input("Enter P's Coefficients: ")

	validinput = inputvalidation(araw, braw, praw)

	while (validinput == False):
		print "Input is Invalid. Please enter again."
		araw = raw_input("Enter A's Coefficients: ")
		braw = raw_input("Enter B's Coefficients: ")
		praw = raw_input("Enter P's Coefficients: ")
		validinput = inputvalidation(araw, braw, praw)

	if (validinput == True):
		print "Valid Input!"
		astr = araw.split(" ")
		bstr = braw.split(" ")
		pstr = praw.split(" ")
		a = []
		b = []
		p = []
		for x in astr:
			if len(x)>0:
				a.append(int(x))
		for x in bstr:
			if len(x)>0:
				b.append(int(x))
		for x in pstr:
			if len(x)>0:
				p.append(int(x))
		print "A(x): ", printpoly(a)
		print "B(x): ", printpoly(b)
		print "P(x): ", printpoly(p)
		a,b = stuff(a,b)
		if (decision == 1):
			print "\nA(x) + B(x):"
			add(a,b)
		elif (decision == 2):
			print "\nA(x) - B(x):"
			print "Subtraction is very much the same as addition!"
			subtract(a,b)
		elif (decision == 3):
			print "\nA(x) x B(x):"
			multiply(a,b,p)